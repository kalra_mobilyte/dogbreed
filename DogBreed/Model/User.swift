//
//  User.swift
//  DogBreed
//
//  Created by Chandan Taneja on 10/04/18.
//  Copyright © 2018 Chandan Taneja. All rights reserved.
//

import Foundation
struct  User:Codable
{
    var status:Bool?
    var userid:Int?
    var users:Users?
    var password:String?
    var devicetoken:String?
    var message:String?
    
    enum CodingKeys: String, CodingKey {
        case status
        case userid
        case users = "users"
        case password
        case devicetoken
        case message
    }
}
struct Users:Codable {
    var firstname:String?
    var lastname:String?
    var emailid:String?
    var country:String?
    var state:String?
    var city:String?
    var phonenumber:Phonenumber?
    enum CodingKeys: String, CodingKey {
        case firstname
        case lastname
        case emailid
        case country
        case state
        case city
        case phonenumber = "phonenumber"
    }
}
struct Phonenumber:Codable {
    var code:Int?
    var countrycde:String?
    var number:Int?

}



//struct User:Codable
//{
//    var login:String?
//    var id: Int?
//    var url:String?
//    var repos_url: String?
//    var events_url: String?
//    var hooks_url:String??
//    var issues_url: String?
//    var members_url: String?
//    var public_members_url: String?
//    var avatar_url: String?
//    var description: String?
//
//}



