//
//  ServiceManager.swift
//  DogBreed
//
//  Created by Chandan Taneja on 10/04/18.
//  Copyright © 2018 Chandan Taneja. All rights reserved.
//

import Foundation
import Alamofire

class ServiceManager
{
    static var shared = ServiceManager()
    func getData()
    {
    let url = ""
    guard let response = URL(string: url) else{return}
    URLSession.shared.dataTask(with: response) { (data, response, error) in
    
    if error != nil
    {
    print("Response not getting")
    }
    else{
    do{
    let stations = try JSONDecoder().decode([User].self, from: data!)
    print(stations)
    }
    catch{
    print("eror")
    }
    
    }
    }.resume()
    
    }
    func loginUser(email:String,password:String,firstName:String,lastName:String,country:String,state:String,city:String,phonenumber:String,confirmPassword:String,devicetoken:String,completionHandler:@escaping (_ successful:Bool,_ user:User?,_ errorMessage:String?)->())
    {
    
        
        self.validateUser(email: email, password: password,firstName:firstName,lastName:lastName,country:country,state:state,city:city,phonenumber:phonenumber,confirmPassword:confirmPassword,devicetoken:devicetoken){ (success,user,errorMessage) in
            if success
            {
                // Credentials are valid...get user details
                self.saveUser(user: user!)
                completionHandler(true, user, "Login Successful")
                
                
            }else
            {
                completionHandler(false, nil, "Incorrect Username or Password")
            }
        }
        
    }
    private func validateUser(email:String,password:String,firstName:String,lastName:String,country:String,state:String,city:String,phonenumber:String,confirmPassword:String,devicetoken:String, completionHandler:@escaping (_ successful:Bool,_ user:User?,_ errorMessage:String?)->())
    {
        
        Alamofire.request("http://10.20.1.15:88/api/Account/Register", method:.post, parameters: ["email" : email,"password":password,"firstName":firstName,"lastName":lastName,"country":country,"state":state,"city":city,"phonenumber":phonenumber,"confirmPassword":confirmPassword,"devicetoken":devicetoken])
            .responseJSON { response in
                
                if response.response?.statusCode == 200
                {
                    if let responseDict = response.result.value as? [String:Any]
                    {
                        print(responseDict)
                        do{
                            let responseData = responseDict["Message"] as? String
                            
                            completionHandler(true, nil,"\(responseData)")
                        }
                        catch{
                            completionHandler(false, nil, "Could not connect to the server")
                        }
                    }
                    else
                    {
                        completionHandler(false, nil, "Could not connect to the server")
                    }
                }
                else
                {
                    completionHandler(false, nil, "Invalid Username or Password")
                }
        }
    }
    //MARK:saved logedIn user
    func saveUser(user:User)
    {
        let userDefaults = UserDefaults.standard
        let storeData = NSKeyedArchiver.archivedData(withRootObject: user)
        userDefaults.set(storeData, forKey: "LoggedInUser")
        userDefaults.synchronize()
    }
    //MARK:Logout method
    func logoutUser()
    {
        if let user = self.getLoggedInUser()
        {
            unRegisterUser("user.accessToken", "user.deviceToken!", completionHandler: { (success) in
                
            })
        }
        let userDefaults = UserDefaults.standard
        userDefaults.setValue(nil, forKey: "LoggedInUser")
        userDefaults.synchronize()
    
    }

    //MARK:Get LogedIn User
    func getLoggedInUser() -> User?
    {
        let userDefaults = UserDefaults.standard
        if let storeData = userDefaults.object(forKey: "LoggedInUser") as? NSData
        {
            return NSKeyedUnarchiver.unarchiveObject(with: storeData as Data) as? User
        }
        else
        {
            return nil
        }
    }
    //MARK:Unregister User
    
    func unRegisterUser(_ accessToken: String, _ deviceToken: String, completionHandler:@escaping (_ successful:Bool)->())
    {
    }
    
}
