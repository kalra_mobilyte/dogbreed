//
//  Utility.swift
//  DogBreed
//
//  Created by Chandan Taneja on 10/04/18.
//  Copyright © 2018 Chandan Taneja. All rights reserved.
//

import Foundation
import UIKit

extension UIView
{
    func roundImage()
    {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
    
    func customButton(){
        self.layer.cornerRadius = 10
        
    }
    
}

extension UINavigationBar{
    
    func hideNavigationBarBottomLine(){
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
    }
}



