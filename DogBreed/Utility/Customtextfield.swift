//
//  Customtextfield.swift
//  DogBreed
//
//  Created by Chandan Taneja on 12/04/18.
//  Copyright © 2018 Chandan Taneja. All rights reserved.
//

import UIKit
class Customtextfield: UITextField
{
        required init?(coder aDecoder: NSCoder)
        {
             super.init(coder: aDecoder)
             layer.borderColor = UIColor.white.cgColor
             layer.borderWidth = 1.0
             layer.cornerRadius = 10.0
        }
}
