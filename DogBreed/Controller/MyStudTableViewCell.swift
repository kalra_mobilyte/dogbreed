//
//  MyStudTableViewCell.swift
//  DogBreed
//
//  Created by Rahul Kalra on 13/04/18.
//  Copyright © 2018 Chandan Taneja. All rights reserved.
//

import UIKit

class MyStudTableViewCell: UITableViewCell {

    @IBOutlet weak var MyStudImage: UIImageView!
    
    @IBOutlet weak var MyStudHeadingText: UILabel!
    
    @IBOutlet weak var MyStudDescriptionText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
