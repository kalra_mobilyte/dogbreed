//
//  ForgotVC.swift
//  DogBreed
//
//  Created by Chandan Taneja on 09/04/18.
//  Copyright © 2018 Chandan Taneja. All rights reserved.
//

import UIKit

class ForgotVC: UIViewController {

    //MARK:- OUTLETS
    
    @IBOutlet weak var submit_BTN: UIButton!
    
    //MARK:- VARIABLES AND CONSTANTS
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        submit_BTN.customButton()
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        
        if(height >= 812.0)
        {
//            scrollView.isScrollEnabled = false
        }
        // Do any additional setup after loading the view.
    }

    
    //MARK:- ACTIONS
    
    @IBAction func onClick_Login(_ sender: Any) {
       self.navigationController?.popViewController(animated:true)
    }
}
