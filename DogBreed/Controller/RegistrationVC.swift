//
//  RegistrationVC.swift
//  DogBreed
//
//  Created by Chandan Taneja on 09/04/18.
//  Copyright © 2018 Chandan Taneja. All rights reserved.
//

import UIKit

class RegistrationVC: UIViewController {
// Outlets
    
    @IBOutlet weak var registration_BTN: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var firstName_TF: Customtextfield!
    
    @IBOutlet weak var confirmPassword_TF: Customtextfield!
    @IBOutlet weak var setPassword_TF: Customtextfield!
    @IBOutlet weak var mobile_TF: Customtextfield!
    @IBOutlet weak var city_TF: Customtextfield!
    @IBOutlet weak var state_TF: Customtextfield!
    @IBOutlet weak var country_TF: Customtextfield!
    @IBOutlet weak var email_TF: Customtextfield!
    @IBOutlet weak var lastName_TF: Customtextfield!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        self.registration_BTN.customButton()
        if(height >= 812.0)
        {
            scrollView.isScrollEnabled = false
        }
// Make scroll view scroll for iphone 5 only
//        self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 504)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- Button Action
    
    @IBAction func onClick_Login(_ sender: Any) {
        self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func onClick_Registration(_ sender: Any) {
        ServiceManager.shared.loginUser(email: "abc@yopmail.com", password: "123456", firstName: "abc", lastName: "abcv", country: "INDIA", state: "Punjab", city: "Mohali", phonenumber: "+91-999999999999", confirmPassword:"123456", devicetoken: "") { (true, user, message) in
            
            print("API successfullyyy hit")
            
        }
        
        
        
        
    }
    
}
