//
//  DashboardVC.swift
//  DogBreed
//
//  Created by Chandan Taneja on 10/04/18.
//  Copyright © 2018 Chandan Taneja. All rights reserved.
//

import UIKit

class HomeVC: UIViewController,UIPopoverPresentationControllerDelegate{
   
    
    //MARK:- Outlets
    
    @IBOutlet weak var dogsImage_CV: UICollectionView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var catagory_BTN: UIButton!
    
     var pickerData: [String] = [String]()
    override func viewDidLoad()
    {
        super.viewDidLoad()
      
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.isHidden = true
        pickerData = ["Catagory1", "Catagory2", "Catagory3", "Catagory4", "Catagory5", "Catagory6"]
        self.catagory_BTN.setTitle("\(pickerData[0])", for: .normal)
    }
    //MARK: Catagory change
    
    @IBAction func onClick_CatagoryChange(_ sender: UIButton)
    {
        self.pickerView.isHidden = false
    }
    
    override func viewWillLayoutSubviews() {
        dogsImage_CV.roundImage()
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}


extension HomeVC:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! CollectionViewCell
        cell.roundImage()
        cell.imageView.roundImage()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
    }
    
    
    
}
extension HomeVC:UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK:Picker View Delegates
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.catagory_BTN.setTitle("\(pickerData[row])", for: .normal)
        self.pickerView.isHidden = true
    }
}
