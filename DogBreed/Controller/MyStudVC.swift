//
//  MyStudVC.swift
//  DogBreed
//
//  Created by Rahul Kalra on 13/04/18.
//  Copyright © 2018 Chandan Taneja. All rights reserved.
//

import UIKit

class MyStudVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
}
extension MyStudVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  10
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyStudTableViewCell", for: indexPath) as? MyStudTableViewCell
        cell?.MyStudImage.roundImage()
        return cell!
        
}

}
