//
//  CollectionViewCell.swift
//  DogBreed
//
//  Created by Chandan Taneja on 11/04/18.
//  Copyright © 2018 Chandan Taneja. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
}
