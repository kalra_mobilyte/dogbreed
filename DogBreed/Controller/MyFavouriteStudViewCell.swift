//
//  MyFavouriteStudViewCell.swift
//  DogBreed
//
//  Created by Rahul Kalra on 14/04/18.
//  Copyright © 2018 Chandan Taneja. All rights reserved.
//

import UIKit

class MyFavouriteStudViewCell: UITableViewCell {
    
    @IBOutlet weak var MyFavStudImage: UIImageView!
    
    @IBOutlet weak var MyFavStudHeadingText: UILabel!
    
    @IBOutlet weak var MyFavStudDescriptionText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
